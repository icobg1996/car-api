package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "EQUIPMENT")
public class EquipmentEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @ManyToMany
    private List<CarEntity> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    public static Equipment toDto(final EquipmentEntity equipmentEntity){
        if(equipmentEntity == null){
            return null;
        }
        final Equipment equipment = toSampleDto(equipmentEntity);
        if(!CollectionUtils.isEmpty(equipmentEntity.getCars())){
            equipment.setCars(equipmentEntity.getCars()
                    .stream()
                    .map(CarEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return equipment;
    }

    public static Equipment toSampleDto(final EquipmentEntity equipmentEntity) {
        if(equipmentEntity == null){
            return null;
        }
        final Equipment equipment = new Equipment();
        equipment.setId(equipmentEntity.getId());
        equipment.setName(equipmentEntity.getName());
        return equipment;
    }

    public static EquipmentEntity toEntity(final Equipment equipment){
        if(equipment == null){
            return null;
        }
        final EquipmentEntity equipmentEntity = toSampleEntity(equipment);
        if(!CollectionUtils.isEmpty(equipment.getCars())){
            equipmentEntity.setCars(equipment.getCars()
                    .stream()
                    .map(CarEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return equipmentEntity;
    }

    public static EquipmentEntity toSampleEntity(final Equipment equipment) {
        if(equipment == null){
            return null;
        }
        final EquipmentEntity equipmentEntity = new EquipmentEntity();
        equipmentEntity.setId(equipment.getId());
        equipmentEntity.setName(equipment.getName());
        return equipmentEntity;

    }
}
