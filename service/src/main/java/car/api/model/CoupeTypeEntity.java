package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "COUPE_TYPE")
public class CoupeTypeEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "TYPE", unique = true, nullable = false)
    private String type;
    @OneToMany(mappedBy = CarEntity_.COUPE_TYPE)
    private List<CarEntity> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    public static CoupeType toDto(final CoupeTypeEntity coupeTypeEntity){
        if(coupeTypeEntity == null){
            return null;
        }
        final CoupeType coupeType = toSampleDto(coupeTypeEntity);
        if(!CollectionUtils.isEmpty(coupeTypeEntity.getCars())){
            coupeType.setCars(coupeTypeEntity.getCars()
                    .stream()
                    .map(CarEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return coupeType;
    }

    public static CoupeType toSampleDto(final CoupeTypeEntity coupeTypeEntity){
        if(coupeTypeEntity == null){
            return null;
        }
        final CoupeType coupeType = new CoupeType();
        coupeType.setId(coupeTypeEntity.getId());
        coupeType.setType(coupeTypeEntity.getType());
        return coupeType;
    }
    public static CoupeTypeEntity toEntity(final CoupeType coupeType){
        if(coupeType == null){
            return null;
        }
        final CoupeTypeEntity coupeTypeEntity = toSampleEntity(coupeType);
        if(!CollectionUtils.isEmpty(coupeType.getCars())){
            coupeTypeEntity.setCars(coupeType.getCars()
                    .stream()
                    .map(CarEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return coupeTypeEntity;
    }

    public static CoupeTypeEntity toSampleEntity(final CoupeType coupeType) {
        if(coupeType == null){
            return null;
        }
        final CoupeTypeEntity coupeTypeEntity = new CoupeTypeEntity();
        coupeTypeEntity.setId(coupeType.getId());
        coupeTypeEntity.setType(coupeType.getType());
        return coupeTypeEntity;
    }
}
