package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "BRAND")
public class BrandEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @ManyToOne
    private CountryEntity countryOfProduction;
    @OneToMany(mappedBy = ModelEntity_.BRAND)
    private List<ModelEntity> models;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryEntity getCountryOfProduction() {
        return countryOfProduction;
    }

    public void setCountryOfProduction(CountryEntity countryOfProduction) {
        this.countryOfProduction = countryOfProduction;
    }

    public List<ModelEntity> getModels() {
        return models;
    }

    public void setModels(List<ModelEntity> models) {
        this.models = models;
    }

    public static Brand toDto(final BrandEntity brandEntity){
        if(brandEntity == null){
            return null;
        }
        final Brand brand = toSampleDto(brandEntity);
        if(!CollectionUtils.isEmpty(brandEntity.getModels())){
            brand.setModels(brandEntity.getModels()
                    .stream()
                    .map(ModelEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return brand;
    }
    public static Brand toSampleDto(final BrandEntity brandEntity){
        if(brandEntity == null){
            return null;
        }
        final Brand brand = new Brand();
        brand.setId(brandEntity.getId());
        brand.setName(brandEntity.getName());
        brand.setCountryOfProduction(CountryEntity.toSampleDto(brandEntity.getCountryOfProduction()));
        return brand;
    }
    public static BrandEntity toEntity(final Brand brand){
        if(brand == null){
            return null;
        }
        final BrandEntity brandEntity = toSampleEntity(brand);
        if(!CollectionUtils.isEmpty(brandEntity.getModels())){
            brandEntity.setModels(brand.getModels()
                    .stream()
                    .map(ModelEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return brandEntity;
    }
    public static BrandEntity toSampleEntity(final Brand brand){
        if(brand == null){
            return null;
        }
        final BrandEntity brandEntity = new BrandEntity();
        brandEntity.setId(brand.getId());
        brandEntity.setName(brand.getName());
        brandEntity.setCountryOfProduction(CountryEntity.toSampleEntity(brand.getCountryOfProduction()));
        return brandEntity;
    }
}
