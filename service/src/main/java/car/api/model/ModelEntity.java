package car.api.model;

import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "MODEL")
public class ModelEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name ="TITLE", nullable = false)
    private String title;
    @ManyToOne
    private BrandEntity brand;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public void setBrand(BrandEntity brand) {
        this.brand = brand;
    }

    public static Model toDto(final ModelEntity modelEntity){
        if (modelEntity == null){
            return null;
        }
        final Model model = toSampleDto(modelEntity);
        return model;
    }

    public static Model toSampleDto(final ModelEntity modelEntity) {
        if (modelEntity == null){
            return null;
        }
        final Model model = new Model();
        model.setId(modelEntity.getId());
        model.setTitle(modelEntity.getTitle());
        model.setBrand(BrandEntity.toSampleDto(modelEntity.getBrand()));
        return model;
    }
    public static ModelEntity toEntity(final Model model){
        if(model == null){
            return null;
        }
        final ModelEntity modelEntity = toSampleEntity(model);
        return modelEntity;
    }

    public static ModelEntity toSampleEntity(final Model model) {
        if(model == null){
            return null;
        }
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.setId(model.getId());
        modelEntity.setTitle(model.getTitle());
        modelEntity.setBrand(BrandEntity.toSampleEntity(model.getBrand()));
        return modelEntity;
    }
}
