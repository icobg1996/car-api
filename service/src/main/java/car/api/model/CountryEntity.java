package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "COUNTRY")
public class CountryEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @OneToMany(mappedBy = BrandEntity_.COUNTRY_OF_PRODUCTION)
    private List<BrandEntity> brands;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BrandEntity> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandEntity> brands) {
        this.brands = brands;
    }

    public static Country toDto(final CountryEntity countryEntity){
        if(countryEntity == null){
            return null;
        }
        final Country country = toSampleDto(countryEntity);
        if(!CollectionUtils.isEmpty(countryEntity.getBrands())){
            country.setBrands(countryEntity.getBrands()
                    .stream()
                    .map(BrandEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return country;
    }
    public static Country toSampleDto(final CountryEntity countryEntity){
        if(countryEntity == null){
            return null;
        }
        final Country country = new Country();
        country.setId(countryEntity.getId());
        country.setName(countryEntity.getName());
        return country;
    }
    public static CountryEntity toEntity(final Country country){
        if(country == null){
            return null;
        }
        final CountryEntity countryEntity = toSampleEntity(country);
        if(!CollectionUtils.isEmpty(country.getBrands())){
            countryEntity.setBrands(country.getBrands()
                    .stream()
                    .map(BrandEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return countryEntity;
    }
    public static CountryEntity toSampleEntity(final Country country){
        if(country == null){
            return null;
        }
        final CountryEntity countryEntity = new CountryEntity();
        countryEntity.setId(country.getId());
        countryEntity.setName(country.getName());
        return countryEntity;
    }
}
