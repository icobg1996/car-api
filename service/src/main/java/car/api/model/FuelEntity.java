package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "FUEL")
public class FuelEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "TYPE", unique = true, nullable = false)
    private String type;
    @OneToMany(mappedBy = CarEntity_.FUEL)
    private List<CarEntity> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    public static Fuel toDto(final FuelEntity fuelEntity){
        if(fuelEntity == null){
            return null;
        }
        final Fuel fuel = toSampleDto(fuelEntity);
        if(!CollectionUtils.isEmpty(fuelEntity.getCars())){
            fuel.setCars(fuelEntity.getCars()
                    .stream()
                    .map(CarEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return fuel;
    }

    public static Fuel toSampleDto(final FuelEntity fuelEntity) {
        if(fuelEntity == null){
            return null;
        }
        final Fuel fuel = new Fuel();
        fuel.setId(fuelEntity.getId());
        fuel.setType(fuelEntity.getType());
        return fuel;
    }

    public static FuelEntity toEntity(final Fuel fuel){
        if(fuel == null){
            return null;
        }
        final FuelEntity fuelEntity = toSampleEntity(fuel);
        if(!CollectionUtils.isEmpty(fuel.getCars())){
            fuelEntity.setCars(fuel.getCars()
                    .stream()
                    .map(CarEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return fuelEntity;
    }

    public static FuelEntity toSampleEntity(final Fuel fuel) {
        if(fuel == null){
            return null;
        }
        final FuelEntity fuelEntity = new FuelEntity();
        fuelEntity.setId(fuel.getId());
        fuelEntity.setType(fuel.getType());
        return fuelEntity;
    }

}
