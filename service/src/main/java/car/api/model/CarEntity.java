package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "CAR")
public class CarEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Min(value = 0)
    @Column(name = "MILEAGE", nullable = false)
    private Integer mileage;
    @Min(value = 0)
    @Column(name = "HORSE_POWER", nullable = false)
    private Short horsePower;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "COLOR")
    private String color;
    @Column(name = "PRODUCTION_YEAR", nullable = false)
    private Integer productionYear;
    @Column(name = "IMAGE_PATH", nullable = false)
    private String imagePath;
    @ManyToOne
    @JoinColumn(name = CarEntity_.MODEL, referencedColumnName = ModelEntity_.ID)
    private ModelEntity model;
    @ManyToOne
    private CoupeTypeEntity coupeType;
    @ManyToOne
    private TransmissionEntity transmission;
    @ManyToOne
    private FuelEntity fuel;
    @ManyToMany
    private List<EquipmentEntity> equipment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Short getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Short horsePower) {
        this.horsePower = horsePower;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Integer productionYear) {
        this.productionYear = productionYear;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ModelEntity getModel() {
        return model;
    }

    public void setModel(ModelEntity model) {
        this.model = model;
    }

    public CoupeTypeEntity getCoupeType() {
        return coupeType;
    }

    public void setCoupeType(CoupeTypeEntity coupeType) {
        this.coupeType = coupeType;
    }

    public TransmissionEntity getTransmission() {
        return transmission;
    }

    public void setTransmission(TransmissionEntity transmission) {
        this.transmission = transmission;
    }

    public FuelEntity getFuel() {
        return fuel;
    }

    public void setFuel(FuelEntity fuel) {
        this.fuel = fuel;
    }

    public List<EquipmentEntity> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<EquipmentEntity> equipment) {
        this.equipment = equipment;
    }

    public static Car toDto(final CarEntity carEntity){
        if(carEntity == null){
            return null;
        }
        final Car car = toSampleDto(carEntity);
        car.setCoupeType(CoupeTypeEntity.toSampleDto(carEntity.getCoupeType()));
        car.setFuel(FuelEntity.toSampleDto(carEntity.getFuel()));
        car.setModel(ModelEntity.toSampleDto(carEntity.getModel()));
        car.setTransmission(TransmissionEntity.toSampleDto(carEntity.getTransmission()));
        if(!CollectionUtils.isEmpty(carEntity.getEquipment())){
            car.setEquipment(carEntity.getEquipment()
                    .stream()
                    .map(EquipmentEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return car;
    }
    public static Car toSampleDto(final CarEntity carEntity){
        if(carEntity == null){
            return null;
        }
        final Car car = new Car();
        car.setId(carEntity.getId());
        car.setColor(carEntity.getColor());
        car.setDescription(carEntity.getDescription());
        car.setMileage(carEntity.getMileage());
        car.setHorsePower(carEntity.getHorsePower());
        car.setProductionYear(carEntity.getProductionYear());
        car.setImagePath(carEntity.getImagePath());
        return car;
    }
    public static CarEntity toEntity(final Car car){
        if(car == null){
            return null;
        }
        final CarEntity carEntity = toSampleEntity(car);
        carEntity.setCoupeType(CoupeTypeEntity.toSampleEntity(car.getCoupeType()));
        carEntity.setFuel(FuelEntity.toSampleEntity(car.getFuel()));
        carEntity.setModel(ModelEntity.toSampleEntity(car.getModel()));
        carEntity.setTransmission(TransmissionEntity.toSampleEntity(car.getTransmission()));
        if(!CollectionUtils.isEmpty(car.getEquipment())){
            carEntity.setEquipment(car.getEquipment()
                    .stream()
                    .map(EquipmentEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return carEntity;
    }
    public static CarEntity toSampleEntity(final Car car){
        if(car == null){
            return null;
        }
        final CarEntity carEntity = new CarEntity();
        carEntity.setId(car.getId());
        carEntity.setColor(car.getColor());
        carEntity.setDescription(car.getDescription());
        carEntity.setMileage(car.getMileage());
        carEntity.setHorsePower(car.getHorsePower());
        carEntity.setProductionYear(car.getProductionYear());
        carEntity.setImagePath(car.getImagePath());
        return carEntity;
    }

}
