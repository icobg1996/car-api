package car.api.model;

import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "TRANSMISSION")
public class TransmissionEntity {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "TYPE", unique = true, nullable = false)
    private String type;
    @OneToMany(mappedBy = CarEntity_.TRANSMISSION)
    private List<CarEntity> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    public static Transmission toDto(final TransmissionEntity transmissionEntity){
        if(transmissionEntity == null){
            return null;
        }
        final Transmission transmission = toSampleDto(transmissionEntity);
        if(!CollectionUtils.isEmpty(transmissionEntity.getCars())){
            transmission.setCars(transmissionEntity.getCars()
                    .stream()
                    .map(CarEntity::toSampleDto)
                    .collect(Collectors.toList()));
        }
        return transmission;
    }

    public static Transmission toSampleDto(final TransmissionEntity transmissionEntity) {
        if(transmissionEntity == null){
            return null;
        }
        final Transmission transmission = new Transmission();
        transmission.setId(transmissionEntity.getId());
        transmission.setType(transmissionEntity.getType());
        return transmission;
    }
    public static TransmissionEntity toEntity(final Transmission transmission){
        if(transmission == null){
            return null;
        }
        final TransmissionEntity transmissionEntity = toSampleEntity(transmission);
        transmissionEntity.setId(transmission.getId());
        transmissionEntity.setType(transmission.getType());
        if(!CollectionUtils.isEmpty(transmission.getCars())){
            transmissionEntity.setCars(transmission.getCars()
                    .stream()
                    .map(CarEntity::toSampleEntity)
                    .collect(Collectors.toList()));
        }
        return transmissionEntity;

    }

    public static TransmissionEntity toSampleEntity(final Transmission transmission) {
        if(transmission == null){
            return null;
        }
        final TransmissionEntity transmissionEntity = new TransmissionEntity();
        transmissionEntity.setId(transmission.getId());
        transmissionEntity.setType(transmission.getType());
        return transmissionEntity;
    }
}
