package car.api;

import car.api.model.BrandEntity;
import car.api.model.CarEntity;
import car.api.model.CountryEntity;
import car.api.model.CoupeTypeEntity;
import car.api.model.EquipmentEntity;
import car.api.model.FuelEntity;
import car.api.model.ModelEntity;
import car.api.model.TransmissionEntity;
import car.api.repository.BrandRepository;
import car.api.repository.CarRepository;
import car.api.repository.CountryRepository;
import car.api.repository.CoupeTypeRepository;
import car.api.repository.EquipmentRepository;
import car.api.repository.FuelRepository;
import car.api.repository.ModelRepository;
import car.api.repository.TransmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Arrays;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private CoupeTypeRepository coupeTypeRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private FuelRepository fuelRepository;
    @Autowired
    private TransmissionRepository transmissionRepository;
    @Autowired
    private EquipmentRepository equipmentRepository;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private ModelRepository modelRepository;
    @Autowired
    private CarRepository carRepository;


    @PostConstruct
    public void postConstruct(){
//        CountryEntity cou1 = new CountryEntity();
//        cou1.setName("Germany");
//        CountryEntity cou2 = new CountryEntity();
//        cou2.setName("France");
//        countryRepository.saveAll(Arrays.asList(cou1,cou2));
//       // countryRepository.flush();
//        cou1 = countryRepository.findByName("Germany").get();
//        cou2 = countryRepository.findByName("France").get();
//
//        CoupeTypeEntity ct1 = new CoupeTypeEntity();
//        ct1.setType("Sedan");
//        CoupeTypeEntity ct2 = new CoupeTypeEntity();
//        ct2.setType("Hatchback");
//        CoupeTypeEntity ct3 = new CoupeTypeEntity();
//        ct3.setType("Combi");
//        CoupeTypeEntity ct4 = new CoupeTypeEntity();
//        ct4.setType("Jeep");
//        coupeTypeRepository.saveAll(Arrays.asList(ct1,ct2,ct3,ct4));
//       // coupeTypeRepository.flush();
//        ct1 = coupeTypeRepository.findByType("Sedan").get();
//        ct2 = coupeTypeRepository.findByType("Hatchback").get();
//        ct3 = coupeTypeRepository.findByType("Combi").get();
//        ct4 = coupeTypeRepository.findByType("Jeep").get();
//
//        FuelEntity f1 = new FuelEntity();
//        f1.setType("Petrol");
//        FuelEntity f2 = new FuelEntity();
//        f2.setType("Diesel");
//        FuelEntity f3 = new FuelEntity();
//        f3.setType("Gas");
//        fuelRepository.saveAll(Arrays.asList(f1,f2,f3));
//       // fuelRepository.flush();
//        f1 = fuelRepository.findByType("Petrol").get();
//        f2 = fuelRepository.findByType("Diesel").get();
//        f3 = fuelRepository.findByType("Gas").get();
//
//        TransmissionEntity t1 = new TransmissionEntity();
//        t1.setType("Automatic");
//        TransmissionEntity t2 = new TransmissionEntity();
//        t2.setType("Manual");
//        transmissionRepository.saveAll(Arrays.asList(t1,t2));
//        //transmissionRepository.flush();
//        t1 = transmissionRepository.findByType("Automatic").get();
//        t2 = transmissionRepository.findByType("Manual").get();
//
//        EquipmentEntity eq1 = new EquipmentEntity();
//        eq1.setName("ABS");
//        EquipmentEntity eq2 = new EquipmentEntity();
//        eq2.setName("ESP");
//        EquipmentEntity eq3 = new EquipmentEntity();
//        eq3.setName("ASR");
//        EquipmentEntity eq4 = new EquipmentEntity();
//        eq4.setName("Air conditioning");
//        equipmentRepository.saveAll(Arrays.asList(eq1,eq2,eq3,eq4));
//        //equipmentRepository.flush();
//        eq1 = equipmentRepository.findByName("ABS").get();
//        eq2 = equipmentRepository.findByName("ESP").get();
//        eq3 = equipmentRepository.findByName("ASR").get();
//        eq4 = equipmentRepository.findByName("Air conditioning").get();
//
//        BrandEntity b1 = new BrandEntity();
//        b1.setName("Mercedes");
//        b1.setCountryOfProduction(cou1);
//        BrandEntity b2 = new BrandEntity();
//        b2.setName("BMW");
//        b2.setCountryOfProduction(cou1);
//        BrandEntity b3 = new BrandEntity();
//        b3.setName("AUDI");
//        b3.setCountryOfProduction(cou1);
//        BrandEntity b4 = new BrandEntity();
//        b4.setName("Renault");
//        b4.setCountryOfProduction(cou2);
//        BrandEntity b5 = new BrandEntity();
//        b5.setName("Peugeot");
//        b5.setCountryOfProduction(cou2);
//        BrandEntity b6 = new BrandEntity();
//        b6.setName("Citroen");
//        b6.setCountryOfProduction(cou2);
//        brandRepository.saveAll(Arrays.asList(b1,b2,b3,b4,b5,b6));
//        //brandRepository.flush();
//        b1 = brandRepository.findByName("Mercedes").get();
//        b2 = brandRepository.findByName("BMW").get();
//        b3 = brandRepository.findByName("AUDI").get();
//        b4 = brandRepository.findByName("Renault").get();
//        b5 = brandRepository.findByName("Peugeot").get();
//        b6 = brandRepository.findByName("Citroen").get();
//
//        ModelEntity m1 = new ModelEntity();
//        m1.setTitle("C 180");
//        m1.setBrand(b1);
//        ModelEntity m2 = new ModelEntity();
//        m2.setTitle("S 500");
//        m2.setBrand(b1);
//        ModelEntity m3 = new ModelEntity();
//        m3.setTitle("318");
//        m3.setBrand(b2);
//        ModelEntity m4 = new ModelEntity();
//        m4.setTitle("507");
//        m4.setBrand(b2);
//        ModelEntity m5 = new ModelEntity();
//        m5.setTitle("A3");
//        m5.setBrand(b3);
//        ModelEntity m6 = new ModelEntity();
//        m6.setTitle("A6");
//        m6.setBrand(b3);
//        ModelEntity m7 = new ModelEntity();
//        m7.setTitle("Megan");
//        m7.setBrand(b4);
//        ModelEntity m8 = new ModelEntity();
//        m8.setTitle("Laguna");
//        m8.setBrand(b4);
//        ModelEntity m9 = new ModelEntity();
//        m9.setTitle("206");
//        m9.setBrand(b5);
//        ModelEntity m10 = new ModelEntity();
//        m10.setTitle("407");
//        m10.setBrand(b5);
//        ModelEntity m11 = new ModelEntity();
//        m11.setTitle("C4");
//        m11.setBrand(b6);
//        ModelEntity m12 = new ModelEntity();
//        m12.setTitle("Xsara");
//        m12.setBrand(b6);
//        modelRepository.saveAll(Arrays.asList(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12));
//       // modelRepository.flush();
//        m1 = modelRepository.findByTitle("C 180").get();
//        m2 = modelRepository.findByTitle("S 500").get();
//        m3 = modelRepository.findByTitle("318").get();
//        m4 = modelRepository.findByTitle("507").get();
//        m5 = modelRepository.findByTitle("A3").get();
//        m6 = modelRepository.findByTitle("A6").get();
//        m7 = modelRepository.findByTitle("Megan").get();
//        m8 = modelRepository.findByTitle("Laguna").get();
//        m9 = modelRepository.findByTitle("206").get();
//        m10 = modelRepository.findByTitle("407").get();
//        m11 = modelRepository.findByTitle("C4").get();
//        m12 = modelRepository.findByTitle("Xsara").get();
//
//        CarEntity c1 = new CarEntity();
//        c1.setColor("Red");
//        c1.setCoupeType(ct1);
//        c1.setEquipment(Arrays.asList(eq1,eq2,eq3,eq4));
//        c1.setFuel(f1);
//        c1.setHorsePower((short) 320);
//        c1.setMileage(20000);
//        c1.setModel(m2);
//        c1.setTransmission(t1);
//        c1.setProductionYear(2018);
//        c1.setImagePath("none");
//        c1.setDescription("Something description");
//
//        CarEntity c2 = new CarEntity();
//        c2.setColor("Blue");
//        c2.setCoupeType(ct2);
//        c2.setEquipment(Arrays.asList(eq1,eq2));
//        c2.setFuel(f3);
//        c2.setHorsePower((short) 125);
//        c2.setMileage(200000);
//        c2.setModel(m5);
//        c2.setTransmission(t2);
//        c2.setProductionYear(2008);
//        c2.setImagePath("none");
//        c2.setDescription("Something description audi");
//
//        CarEntity c3 = new CarEntity();
//        c3.setColor("Black");
//        c3.setCoupeType(ct1);
//        c3.setEquipment(Arrays.asList(eq3,eq4));
//        c3.setFuel(f2);
//        c3.setHorsePower((short) 105);
//        c3.setMileage(250000);
//        c3.setModel(m7);
//        c3.setTransmission(t2);
//        c3.setProductionYear(2006);
//        c3.setImagePath("none");
//        c3.setDescription("Something description renaut");
//        carRepository.saveAll(Arrays.asList(c1,c2,c3));
//        carRepository.flush();

    }

}
