package car.api.utility;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public class PredicateBuilder<ENTITY> {
    private Root<ENTITY> root;
    private CriteriaBuilder builder;
    private List<Predicate> predicates;

    public PredicateBuilder(Root<ENTITY> root, CriteriaBuilder builder){
        this.root = root;
        this.builder = builder;
        this.predicates = new ArrayList<>();
    }
    public PredicateBuilder<ENTITY> in(final SingularAttribute<ENTITY, ?> attribute, final Supplier<Collection<?>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            predicates.add(root.get(attribute).in(supplier.get()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> joinIn(final ListAttribute<ENTITY, ?> attribute, final Supplier<Collection<?>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            final ListJoin<ENTITY, ?> joinColumn = root.join(attribute);
            predicates.add(joinColumn.in(supplier.get()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> equals(final SingularAttribute<ENTITY, ?> attribute, final Supplier<Collection<Object>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(object -> equal(attribute, () -> object));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> equal(final SingularAttribute<ENTITY, ?> attribute, final Supplier<Object> supplier){
        if(supplier.get() != null){
            predicates.add(builder.equal(root.get(attribute), supplier.get()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> equalIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.equal(builder.lower(root.get(attribute)), supplier.get().toLowerCase()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> equalsIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> equalIgnoreCase(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> likes(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(value -> like(attribute, () -> value));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> like(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(root.get(attribute), supplier.get()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> contain(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(root.get(attribute), "%" + supplier.get() + "%"));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> contains(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> contain(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> containIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(builder.lower(root.get(attribute)), "%" + supplier.get().toLowerCase() + "%"));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> containsIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> containIgnoreCase(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> startWith(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(root.get(attribute), "%" + supplier.get()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> startsWith(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> startWith(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> startWithIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(builder.lower(root.get(attribute)), "%" + supplier.get().toLowerCase()));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> startsWithIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> startWithIgnoreCase(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> endWith(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(root.get(attribute), supplier.get() + "%"));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> endsWith(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> endWith(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> endWithIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<String> supplier){
        if(!StringUtils.isEmpty(supplier.get())){
            predicates.add(builder.like(builder.lower(root.get(attribute)), supplier.get().toLowerCase() + "%"));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> endsWithIgnoreCase(final SingularAttribute<ENTITY, String> attribute, final Supplier<Collection<String>> supplier){
        if(!CollectionUtils.isEmpty(supplier.get())){
            supplier.get().forEach(string -> endWithIgnoreCase(attribute, () -> string));
        }
        return this;
    }
    public PredicateBuilder<ENTITY> greaterOrEqual(final SingularAttribute<ENTITY, Integer> attribute, final Supplier<Integer> supplier){
        if(supplier.get() != null){
            predicates.add(builder.greaterThanOrEqualTo(root.get(attribute), supplier.get()));
        }
        return this;
    }
    public List<Predicate> build(){
        return this.predicates;
    }
}
