package car.api.repository;

import car.api.model.FuelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@Repository
public interface FuelRepository extends JpaRepository<FuelEntity,Long> {
    Optional<FuelEntity> findByType (String type);
}
