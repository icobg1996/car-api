package car.api.repository.filter;

import car.api.filter.CarFilter;
import car.api.model.CarEntity;
import car.api.model.CarEntity_;
import car.api.utility.PredicateBuilder;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
/**
 * @Created on 25.11.2019 г.
 * @Author Hristo Ispirov
 */
public class CarSpecification implements Specification<CarEntity> {
    private CarFilter filter;
    public CarSpecification( CarFilter filter){
        this.filter = filter;
    }
    private List<Long> ids;

    private List<Long> transmissionIds;
    private List<Long> fuelIds;
    @Override
    public Predicate toPredicate(Root<CarEntity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        final PredicateBuilder<CarEntity> predicateBuilder = new PredicateBuilder<>(root,criteriaBuilder)
                .in(CarEntity_.id, filter::getIds)
                .in(CarEntity_.model, filter::getModelIds)
                .in(CarEntity_.coupeType, filter::getCoupeTypeIds)
                .in(CarEntity_.transmission, filter::getTransmissionIds)
                .in(CarEntity_.fuel, filter::getFuelIds)
                .joinIn(CarEntity_.equipment, filter::getEquipmentIds)
                .containIgnoreCase(CarEntity_.color, filter::getColor)
                .equal(CarEntity_.horsePower, filter::getHorsePower)
                .greaterOrEqual(CarEntity_.productionYear, filter::getProductionYear)
                .equal(CarEntity_.mileage, filter::getMileage)
                .equalIgnoreCase(CarEntity_.description, filter::getDescription);

        return criteriaQuery
                .distinct(true)
                .where(predicateBuilder
                .build()
                .toArray(new Predicate[0]))
                .getGroupRestriction();
    }

}
