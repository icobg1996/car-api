package car.api.repository;

import car.api.model.ModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@Repository
public interface ModelRepository extends JpaRepository<ModelEntity,Long> {
    Optional<ModelEntity> findByTitle (String title);
    List<ModelEntity> findByBrandId(Long brandId);
}
