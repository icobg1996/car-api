package car.api.repository;

import car.api.model.CoupeTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@Repository
public interface CoupeTypeRepository extends JpaRepository<CoupeTypeEntity, Long> {
    Optional<CoupeTypeEntity> findByType (String type);
}
