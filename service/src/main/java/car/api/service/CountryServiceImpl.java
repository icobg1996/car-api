package car.api.service;

import car.api.model.BrandEntity;
import car.api.model.Country;
import car.api.model.CountryEntity;
import car.api.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<Country> getAll() {
        return countryRepository.findAll()
                .stream()
                .map(CountryEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Country> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<CountryEntity> optionalCountryEntity = countryRepository.findById(id);
        return optionalCountryEntity.map(countryEntity -> new ResponseEntity<>(CountryEntity.toDto(countryEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Country> getByName(final String name) {
        if(StringUtils.isEmpty(name)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<CountryEntity> optionalCountryEntity = countryRepository.findByName(name);
        return optionalCountryEntity.map(countryEntity -> new ResponseEntity<>(CountryEntity.toDto(countryEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Country> insert(final Country country) {
        if(country == null || StringUtils.isEmpty(country.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CountryEntity countryEntity = countryRepository.save(CountryEntity.toEntity(country));
        return new ResponseEntity<>(CountryEntity.toDto(countryEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Country> update(final Country country) {
        if(country == null || country.getId() == null || StringUtils.isEmpty(country.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CountryEntity countryEntity = countryRepository.save(CountryEntity.toEntity(country));
        return new ResponseEntity<>(CountryEntity.toDto(countryEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            countryRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
