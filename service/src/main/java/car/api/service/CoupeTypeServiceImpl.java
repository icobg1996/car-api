package car.api.service;

import car.api.model.CoupeType;
import car.api.model.CoupeTypeEntity;
import car.api.repository.CoupeTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class CoupeTypeServiceImpl implements CoupeTypeService{
    @Autowired
    private CoupeTypeRepository coupeTypeRepository;
    @Override
    public List<CoupeType> getAll() {
        return coupeTypeRepository.findAll()
                .stream()
                .map(CoupeTypeEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<CoupeType> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<CoupeTypeEntity> optionalCoupeTypeEntity = coupeTypeRepository.findById(id);
        return optionalCoupeTypeEntity.map(coupeTypeEntity -> new ResponseEntity<>(CoupeTypeEntity.toDto(coupeTypeEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<CoupeType> getByType(final String type) {
        if(StringUtils.isEmpty(type)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<CoupeTypeEntity> optionalCoupeTypeEntity = coupeTypeRepository.findByType(type);
        return optionalCoupeTypeEntity.map(coupeTypeEntity -> new ResponseEntity<>(CoupeTypeEntity.toDto(coupeTypeEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<CoupeType> insert(final CoupeType coupeType) {
        if(coupeType == null || StringUtils.isEmpty(coupeType.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CoupeTypeEntity coupeTypeEntity = coupeTypeRepository.save(CoupeTypeEntity.toEntity(coupeType));
        return new ResponseEntity<>(CoupeTypeEntity.toDto(coupeTypeEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CoupeType> update(final CoupeType coupeType) {
        if(coupeType == null || coupeType.getId() == null || StringUtils.isEmpty(coupeType.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CoupeTypeEntity coupeTypeEntity = coupeTypeRepository.save(CoupeTypeEntity.toEntity(coupeType));
        return new ResponseEntity<>(CoupeTypeEntity.toDto(coupeTypeEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            coupeTypeRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
