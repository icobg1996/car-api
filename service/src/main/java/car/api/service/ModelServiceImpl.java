package car.api.service;

import car.api.model.Model;
import car.api.model.ModelEntity;
import car.api.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class ModelServiceImpl implements ModelService {
    @Autowired
    private ModelRepository modelRepository;

    @Override
    public List<Model> getAll() {
        return modelRepository.findAll()
                .stream()
                .map(ModelEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Model> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<ModelEntity> optionalModelEntity = modelRepository.findById(id);
        return optionalModelEntity.map(modelEntity -> new ResponseEntity<>(ModelEntity.toDto(modelEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<List<Model>> getByBrandId(Long brandId) {
        if(brandId == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final List<Model> models = modelRepository.findByBrandId(brandId)
                .stream()
                .map(ModelEntity::toDto)
                .collect(Collectors.toList());
        return new ResponseEntity<>(models,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Model> getByTitle(final String title) {
        if(StringUtils.isEmpty(title)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<ModelEntity> optionalModelEntity = modelRepository.findByTitle(title);
        return optionalModelEntity.map(modelEntity -> new ResponseEntity<>(ModelEntity.toDto(modelEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Model> insert(final Model model) {
        if(model == null || StringUtils.isEmpty(model.getTitle())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final ModelEntity modelEntity = modelRepository.save(ModelEntity.toEntity(model));
        return new ResponseEntity<>(ModelEntity.toDto(modelEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Model> update(final Model model) {
        if(model == null || model.getId() == null || StringUtils.isEmpty(model.getTitle() == null)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final ModelEntity modelEntity = modelRepository.save(ModelEntity.toEntity(model));
        return new ResponseEntity<>(ModelEntity.toDto(modelEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            modelRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
