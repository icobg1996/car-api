package car.api.service;

import car.api.model.Brand;
import car.api.model.BrandEntity;
import car.api.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<Brand> getAll() {
        return brandRepository.findAll()
                .stream()
                .map(BrandEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Brand> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<BrandEntity> optionalBrandEntity = brandRepository.findById(id);
        return optionalBrandEntity.map(brandEntity -> new ResponseEntity<>(BrandEntity.toDto(brandEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Brand> getByName(final String name) {
        if(StringUtils.isEmpty(name)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<BrandEntity> optionalBrandEntity = brandRepository.findByName(name);
        return optionalBrandEntity.map(brandEntity -> new ResponseEntity<>(BrandEntity.toDto(brandEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Brand> insert(final Brand brand) {
        if(brand == null || StringUtils.isEmpty(brand.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final BrandEntity brandEntity = brandRepository.save(BrandEntity.toEntity(brand));
        return new ResponseEntity<>(BrandEntity.toDto(brandEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Brand> update(final Brand brand) {
        if(brand == null || brand.getId() == null || StringUtils.isEmpty(brand.getName() == null)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final BrandEntity brandEntity = brandRepository.save(BrandEntity.toEntity(brand));
        return new ResponseEntity<>(BrandEntity.toDto(brandEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            brandRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
