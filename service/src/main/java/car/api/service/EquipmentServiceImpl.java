package car.api.service;

import car.api.model.Equipment;
import car.api.model.EquipmentEntity;
import car.api.repository.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class EquipmentServiceImpl implements EquipmentService {
    @Autowired
    private EquipmentRepository equipmentRepository;

    @Override
    public List<Equipment> getAll() {
        return equipmentRepository.findAll()
                .stream()
                .map(EquipmentEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Equipment> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<EquipmentEntity> optionalEquipmentEntity = equipmentRepository.findById(id);
        return optionalEquipmentEntity.map(equipmentEntity -> new ResponseEntity<>(EquipmentEntity.toDto(equipmentEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Equipment> getByName(final String name) {
        if(StringUtils.isEmpty(name)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<EquipmentEntity> optionalEquipmentEntity = equipmentRepository.findByName(name);
        return optionalEquipmentEntity.map(equipmentEntity -> new ResponseEntity<>(EquipmentEntity.toDto(equipmentEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Equipment> insert(final Equipment equipment) {
        if(equipment == null || StringUtils.isEmpty(equipment.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final EquipmentEntity equipmentEntity = equipmentRepository.save(EquipmentEntity.toEntity(equipment));
        return new ResponseEntity<>(EquipmentEntity.toDto(equipmentEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Equipment> update(final Equipment equipment) {
        if(equipment == null || equipment.getId() == null || StringUtils.isEmpty(equipment.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final EquipmentEntity equipmentEntity = equipmentRepository.save(EquipmentEntity.toEntity(equipment));
        return new ResponseEntity<>(EquipmentEntity.toDto(equipmentEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            equipmentRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
