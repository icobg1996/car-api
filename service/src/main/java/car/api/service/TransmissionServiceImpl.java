package car.api.service;

import car.api.model.Transmission;
import car.api.model.TransmissionEntity;
import car.api.repository.TransmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class TransmissionServiceImpl implements TransmissionService {
    @Autowired
    private TransmissionRepository transmissionRepository;
    
    @Override
    public List<Transmission> getAll() {
        return transmissionRepository.findAll()
                .stream()
                .map(TransmissionEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Transmission> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<TransmissionEntity> optionalTransmissionEntity = transmissionRepository.findById(id);
        return optionalTransmissionEntity.map(transmissionEntity -> new ResponseEntity<>(TransmissionEntity.toDto(transmissionEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Transmission> getByType(final String type) {
        if(StringUtils.isEmpty(type)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<TransmissionEntity> optionalTransmissionEntity = transmissionRepository.findByType(type);
        return optionalTransmissionEntity.map(transmissionEntity -> new ResponseEntity<>(TransmissionEntity.toDto(transmissionEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Transmission> insert(final Transmission transmission) {
        if(transmission == null || StringUtils.isEmpty(transmission.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final TransmissionEntity transmissionEntity = transmissionRepository.save(TransmissionEntity.toEntity(transmission));
        return new ResponseEntity<>(TransmissionEntity.toDto(transmissionEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Transmission> update(final Transmission transmission) {
        if(transmission == null || transmission.getId() == null || StringUtils.isEmpty(transmission.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final TransmissionEntity transmissionEntity = transmissionRepository.save(TransmissionEntity.toEntity(transmission));
        return new ResponseEntity<>(TransmissionEntity.toDto(transmissionEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            transmissionRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
