package car.api.service;

import car.api.filter.CarFilter;
import car.api.model.Car;
import car.api.model.CarEntity;
import car.api.repository.CarRepository;
import car.api.repository.filter.CarSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Created on 25.11.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class CarServiceImpl implements CarService {
    @Autowired
    private CarRepository carRepository;
    @Override
    public List<Car> getCars(CarFilter carFilter) {
        return carRepository.findAll(new CarSpecification(carFilter))
                .stream()
                .map(CarEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Car> insertCar(Car car) {
        if(car == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CarEntity carEntity = carRepository.save(CarEntity.toEntity(car));
        return new ResponseEntity<>(CarEntity.toDto(carEntity),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Car> updateCar(Car car) {
        if(car == null || car.getId() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final CarEntity carEntity = carRepository.save(CarEntity.toEntity(car));
        return new ResponseEntity<>(CarEntity.toDto(carEntity),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteCarById(Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            carRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @Override
    public void singleFileUpload(MultipartFile file) {
        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get("./src/main/webapp/asset/test-" + file.getOriginalFilename());
            Files.write(path, bytes);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
