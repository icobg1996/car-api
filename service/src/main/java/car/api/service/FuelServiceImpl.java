package car.api.service;

import car.api.model.Fuel;
import car.api.model.FuelEntity;
import car.api.repository.FuelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
@RestController
public class FuelServiceImpl implements FuelService {
    @Autowired
    private FuelRepository fuelRepository;
    @Override
    public List<Fuel> getAll() {
        return fuelRepository.findAll()
                .stream()
                .map(FuelEntity::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Fuel> getById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<FuelEntity> optionalFuelEntity = fuelRepository.findById(id);
        return optionalFuelEntity.map(fuelEntity -> new ResponseEntity<>(FuelEntity.toDto(fuelEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Fuel> getByType(final String type) {
        if(StringUtils.isEmpty(type)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final Optional<FuelEntity> optionalFuelEntity = fuelRepository.findByType(type);
        return optionalFuelEntity.map(fuelEntity -> new ResponseEntity<>(FuelEntity.toDto(fuelEntity), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Fuel> insert(final Fuel fuel) {
        if(fuel == null || StringUtils.isEmpty(fuel.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final FuelEntity fuelEntity = fuelRepository.save(FuelEntity.toEntity(fuel));
        return new ResponseEntity<>(FuelEntity.toDto(fuelEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Fuel> update(final Fuel fuel) {
        if(fuel == null || fuel.getId() == null || StringUtils.isEmpty(fuel.getType())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final FuelEntity fuelEntity = fuelRepository.save(FuelEntity.toEntity(fuel));
        return new ResponseEntity<>(FuelEntity.toDto(fuelEntity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(final Long id) {
        if(id == null){
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        try {
            fuelRepository.deleteById(id);
        }catch (final DataIntegrityViolationException e){
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
