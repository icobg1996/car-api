package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EquipmentEntity.class)
public abstract class EquipmentEntity_ {

	public static volatile ListAttribute<EquipmentEntity, CarEntity> cars;
	public static volatile SingularAttribute<EquipmentEntity, String> name;
	public static volatile SingularAttribute<EquipmentEntity, Long> id;

	public static final String CARS = "cars";
	public static final String NAME = "name";
	public static final String ID = "id";

}

