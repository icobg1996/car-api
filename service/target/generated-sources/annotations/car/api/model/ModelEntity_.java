package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelEntity.class)
public abstract class ModelEntity_ {

	public static volatile SingularAttribute<ModelEntity, Long> id;
	public static volatile SingularAttribute<ModelEntity, String> title;
	public static volatile SingularAttribute<ModelEntity, BrandEntity> brand;

	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String BRAND = "brand";

}

