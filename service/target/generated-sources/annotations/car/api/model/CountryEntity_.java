package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CountryEntity.class)
public abstract class CountryEntity_ {

	public static volatile ListAttribute<CountryEntity, BrandEntity> brands;
	public static volatile SingularAttribute<CountryEntity, String> name;
	public static volatile SingularAttribute<CountryEntity, Long> id;

	public static final String BRANDS = "brands";
	public static final String NAME = "name";
	public static final String ID = "id";

}

