package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FuelEntity.class)
public abstract class FuelEntity_ {

	public static volatile ListAttribute<FuelEntity, CarEntity> cars;
	public static volatile SingularAttribute<FuelEntity, Long> id;
	public static volatile SingularAttribute<FuelEntity, String> type;

	public static final String CARS = "cars";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

