package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransmissionEntity.class)
public abstract class TransmissionEntity_ {

	public static volatile ListAttribute<TransmissionEntity, CarEntity> cars;
	public static volatile SingularAttribute<TransmissionEntity, Long> id;
	public static volatile SingularAttribute<TransmissionEntity, String> type;

	public static final String CARS = "cars";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

