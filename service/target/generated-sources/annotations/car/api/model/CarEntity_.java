package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CarEntity.class)
public abstract class CarEntity_ {

	public static volatile SingularAttribute<CarEntity, CoupeTypeEntity> coupeType;
	public static volatile SingularAttribute<CarEntity, Short> horsePower;
	public static volatile SingularAttribute<CarEntity, TransmissionEntity> transmission;
	public static volatile SingularAttribute<CarEntity, String> color;
	public static volatile SingularAttribute<CarEntity, String> imagePath;
	public static volatile SingularAttribute<CarEntity, FuelEntity> fuel;
	public static volatile SingularAttribute<CarEntity, String> description;
	public static volatile ListAttribute<CarEntity, EquipmentEntity> equipment;
	public static volatile SingularAttribute<CarEntity, ModelEntity> model;
	public static volatile SingularAttribute<CarEntity, Long> id;
	public static volatile SingularAttribute<CarEntity, Integer> productionYear;
	public static volatile SingularAttribute<CarEntity, Integer> mileage;

	public static final String COUPE_TYPE = "coupeType";
	public static final String HORSE_POWER = "horsePower";
	public static final String TRANSMISSION = "transmission";
	public static final String COLOR = "color";
	public static final String IMAGE_PATH = "imagePath";
	public static final String FUEL = "fuel";
	public static final String DESCRIPTION = "description";
	public static final String EQUIPMENT = "equipment";
	public static final String MODEL = "model";
	public static final String ID = "id";
	public static final String PRODUCTION_YEAR = "productionYear";
	public static final String MILEAGE = "mileage";

}

