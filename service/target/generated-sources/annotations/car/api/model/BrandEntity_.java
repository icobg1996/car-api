package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BrandEntity.class)
public abstract class BrandEntity_ {

	public static volatile ListAttribute<BrandEntity, ModelEntity> models;
	public static volatile SingularAttribute<BrandEntity, CountryEntity> countryOfProduction;
	public static volatile SingularAttribute<BrandEntity, String> name;
	public static volatile SingularAttribute<BrandEntity, Long> id;

	public static final String MODELS = "models";
	public static final String COUNTRY_OF_PRODUCTION = "countryOfProduction";
	public static final String NAME = "name";
	public static final String ID = "id";

}

