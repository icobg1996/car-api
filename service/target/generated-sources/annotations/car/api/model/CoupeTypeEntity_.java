package car.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CoupeTypeEntity.class)
public abstract class CoupeTypeEntity_ {

	public static volatile ListAttribute<CoupeTypeEntity, CarEntity> cars;
	public static volatile SingularAttribute<CoupeTypeEntity, Long> id;
	public static volatile SingularAttribute<CoupeTypeEntity, String> type;

	public static final String CARS = "cars";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

