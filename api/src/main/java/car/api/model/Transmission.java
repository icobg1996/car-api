package car.api.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
/**
 * @Created on 20.11.2019 г.
 * @Author Hristo Ispirov
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
public class Transmission {
    @EqualsAndHashCode.Include
    private Long id;
    private String type;
    private List<Car> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
