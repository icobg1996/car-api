package car.api.filter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
/**
 * @Created on 25.11.2019 г.
 * @Author Hristo Ispirov
 */
public class CarFilter implements Serializable {
    private List<Long> ids;
    private Integer mileage;
    private Short horsePower;
    private String description;
    private String color;
    private Integer productionYear;
    private List<Long> modelIds;
    private List<Long> coupeTypeIds;
    private List<Long> transmissionIds;
    private List<Long> fuelIds;
    private List<Long> equipmentIds;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Short getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Short horsePower) {
        this.horsePower = horsePower;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Integer productionYear) {
        this.productionYear = productionYear;
    }

    public List<Long> getModelIds() {
        return modelIds;
    }

    public void setModelIds(List<Long> modelIds) {
        this.modelIds = modelIds;
    }

    public List<Long> getCoupeTypeIds() {
        return coupeTypeIds;
    }

    public void setCoupeTypeIds(List<Long> coupeTypeIds) {
        this.coupeTypeIds = coupeTypeIds;
    }

    public List<Long> getTransmissionIds() {
        return transmissionIds;
    }

    public void setTransmissionIds(List<Long> transmissionIds) {
        this.transmissionIds = transmissionIds;
    }

    public List<Long> getFuelIds() {
        return fuelIds;
    }

    public void setFuelIds(List<Long> fuelIds) {
        this.fuelIds = fuelIds;
    }

    public List<Long> getEquipmentIds() {
        return equipmentIds;
    }

    public void setEquipmentIds(List<Long> equipmentIds) {
        this.equipmentIds = equipmentIds;
    }
}
