package car.api.service;

import car.api.model.Equipment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface EquipmentService {
    @GetMapping(path = "equipment/get/all")
    List<Equipment> getAll();

    @GetMapping(path = "equipment/get/by/id")
    ResponseEntity<Equipment> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "equipment/get/by/name")
    ResponseEntity<Equipment> getByName(@RequestParam(name = "name") final String name);

    @PostMapping(path = "equipment/insert")
    ResponseEntity<Equipment> insert(@RequestBody final Equipment equipment);

    @PutMapping(path = "equipment/update")
    ResponseEntity<Equipment> update(@RequestBody final Equipment equipment);

    @DeleteMapping(path = "equipment/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
