package car.api.service;

import car.api.model.Brand;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface BrandService {
    @GetMapping(path = "brand/get/all")
    List<Brand> getAll();

    @GetMapping(path = "brand/get/by/id")
    ResponseEntity<Brand> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "brand/get/by/name")
    ResponseEntity<Brand> getByName(@RequestParam(name = "name") final String name);

    @PostMapping(path = "brand/insert")
    ResponseEntity<Brand> insert(@RequestBody final Brand brand);

    @PutMapping(path = "brand/update")
    ResponseEntity<Brand> update(@RequestBody final Brand brand);

    @DeleteMapping(path = "brand/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
