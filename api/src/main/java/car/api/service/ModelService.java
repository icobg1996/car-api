package car.api.service;

import car.api.model.Model;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface ModelService {
    @GetMapping(path = "model/get/all")
    List<Model> getAll();

    @GetMapping(path = "model/get/by/id")
    ResponseEntity<Model> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "model/get/by/brand/id")
    ResponseEntity<List<Model>> getByBrandId(@RequestParam(name = "brandId") final Long brandId);

    @GetMapping(path = "model/get/by/title")
    ResponseEntity<Model> getByTitle(@RequestParam(name = "title") final String title);

    @PostMapping(path = "model/insert")
    ResponseEntity<Model> insert(@RequestBody final Model model);

    @PutMapping(path = "model/update")
    ResponseEntity<Model> update(@RequestBody final Model model);

    @DeleteMapping(path = "model/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
