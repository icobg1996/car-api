package car.api.service;

import car.api.model.Fuel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface FuelService {
    @GetMapping(path = "fuel/get/all")
    List<Fuel> getAll();

    @GetMapping(path = "fuel/get/by/id")
    ResponseEntity<Fuel> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "fuel/get/by/type")
    ResponseEntity<Fuel> getByType(@RequestParam(name = "type") final String type);

    @PostMapping(path = "fuel/insert")
    ResponseEntity<Fuel> insert(@RequestBody final Fuel fuel);

    @PutMapping(path = "fuel/update")
    ResponseEntity<Fuel> update(@RequestBody final Fuel fuel);

    @DeleteMapping(path = "fuel/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
