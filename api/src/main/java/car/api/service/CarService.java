package car.api.service;

import car.api.filter.CarFilter;
import car.api.model.Car;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Created on 25.11.2019 г.
 * @Author Hristo Ispirov
 */
public interface CarService {
    @GetMapping(path = "car/get/all")
    List<Car> getCars(CarFilter carFilter);
    @PostMapping(path = "car/insert")
    ResponseEntity<Car> insertCar(@RequestBody Car car);
    @PutMapping(path = "car/update")
    ResponseEntity<Car> updateCar(@RequestBody Car car);
    @DeleteMapping(path = "car/delete")
    ResponseEntity<Boolean> deleteCarById(@RequestParam(name = "id") Long id);
    @PostMapping("car/upload") // //new annotation since 4.3
    void singleFileUpload(@RequestParam("file") MultipartFile file);
}
