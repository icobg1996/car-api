package car.api.service;

import car.api.model.Country;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface CountryService {
    @GetMapping(path = "country/get/all")
    List<Country> getAll();

    @GetMapping(path = "country/get/by/id")
    ResponseEntity<Country> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "country/get/by/name")
    ResponseEntity<Country> getByName(@RequestParam(name = "name") final String name);

    @PostMapping(path = "country/insert")
    ResponseEntity<Country> insert(@RequestBody final Country country);

    @PutMapping(path = "country/update")
    ResponseEntity<Country> update(@RequestBody final Country country);

    @DeleteMapping(path = "country/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
