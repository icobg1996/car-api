package car.api.service;

import car.api.model.Transmission;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface TransmissionService {
    @GetMapping(path = "transmission/get/all")
    List<Transmission> getAll();

    @GetMapping(path = "transmission/get/by/id")
    ResponseEntity<Transmission> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "transmission/get/by/type")
    ResponseEntity<Transmission> getByType(@RequestParam(name = "type") final String type);

    @PostMapping(path = "transmission/insert")
    ResponseEntity<Transmission> insert(@RequestBody final Transmission transmission);

    @PutMapping(path = "transmission/update")
    ResponseEntity<Transmission> update(@RequestBody final Transmission transmission);

    @DeleteMapping(path = "transmission/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
