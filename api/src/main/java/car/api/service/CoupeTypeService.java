package car.api.service;

import car.api.model.CoupeType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Created on 4.12.2019 г.
 * @Author Hristo Ispirov
 */
public interface CoupeTypeService {
    @GetMapping(path = "coupe/type/get/all")
    List<CoupeType> getAll();

    @GetMapping(path = "coupe/type/get/by/id")
    ResponseEntity<CoupeType> getById(@RequestParam(name = "id") final Long id);

    @GetMapping(path = "coupe/type/get/by/type")
    ResponseEntity<CoupeType> getByType(@RequestParam(name = "type") final String type);

    @PostMapping(path = "coupe/type/insert")
    ResponseEntity<CoupeType> insert(@RequestBody final CoupeType coupeType);

    @PutMapping(path = "coupe/type/update")
    ResponseEntity<CoupeType> update(@RequestBody final CoupeType coupeType);

    @DeleteMapping(path = "coupe/type/delete/by/id")
    ResponseEntity<Boolean> deleteById(@RequestParam(name = "id") final Long id);
}
