let ajaxRequest = function (url, method, successFunction, data){
    $.ajax({
        url: url,
        method: method,
        contentType: 'application/json',
        data,
        complete: function(response){
            switch(response.status){
                case 200:
                    successFunction(response.responseJSON);
                    break;
                case 401: 
                    window.location.replace('index.html');
                    break;
                case 400:
                    break;
            }   
        }
    });
}

var doGet = (url, successFunction, data) => ajaxRequest(url, 'GET', successFunction, data);
var doPost = (url, successFunction, data) => ajaxRequest(url, 'POST', successFunction, data);
var doPut =  (url, successFunction, data) => ajaxRequest(url, 'PUT', successFunction, data);
var doDelete = (url, successFunction, data) => ajaxRequest(url, 'DELETE', successFunction, data);
   