let updateActiveOption = function (button) {
    $('.option-btn').each(function () {
        $(this).removeClass('active');
    });
    button.addClass('active');
}
$(document).ready(function () {
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $('#option-brand').click(function () {
        updateActiveOption($(this));
        fillBrands();
    });
    $('#option-molel').click(function () {
        updateActiveOption($(this));
        fillModels();
    });
    $('#option-transmission').click(function () {
        updateActiveOption($(this));
        fillTransmission();
    });
    $('#option-coupe-type').click(function () {
        updateActiveOption($(this));
        fillCoupeType();

    });
    $('#option-equipment').click(function () {
        updateActiveOption($(this));
        fillEquipment();
    });
    $('#option-fuel').click(function () {
        updateActiveOption($(this));
        fillFuel();

    });
    $('#option-country').click(function () {
        updateActiveOption($(this));
        fillCountry();

    });
    $('#option-car').click(function () {
        updateActiveOption($(this));
        fillCar();
    });
    $('#save-file').click(function(){
        var data = new FormData();
        data.set('file', $('#customFile')[0].files[0]);
        
        $.ajax({
            url: 'car/upload',
            method: 'POST',
            data: data,
            contentType: false,
            processData: false,
            complete: function(response){
                switch(response.status){
                    case 200:
                        //successFunction(response.responseJSON);
                        break;
                    case 401: 
                        window.location.replace('index.html');
                        break;
                    case 400:
                        break;
                }   
            }
        });
    });
});