let getModelUpdateButton = function(model){
    return createUpdatebutton(function () {
        updateModelModal(model);
        $('#btn-add').trigger('click');
    });

}
let getModelDeletaButton = function(modelId){
    return createDeletebutton(function(){
        doDelete('model/delete/by/id/?id=' + modelId,function(isDelete){
            if(isDelete){
                fillModels();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveModel = function () {
    let titleInput = $('#modal-model');
    titleInput.val('');
    let brandInput = $('#modal-brand');
    brandInput.val("-1")
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let model = {};
        model.title = titleInput.val();
        model.brand = { id: parseInt(brandInput.val())};
        doPost('model/insert', function (model) {
            $('#btn-cancel').trigger('click');
            fillModels();
        }, JSON.stringify(model));
    });
}
let updateModelModal = function (model) {
    let titleInput = $('#modal-model');
    titleInput.val(model.title);
    let brandInput = $('#modal-brand');
    brandInput.val(model.brand.id);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        model.title = titleInput.val();
        model.brand = { id: parseInt(brandInput.val()) };
        doPut('model/update', function (model) {
            $('#btn-cancel').trigger('click');
            fillModels();
        }, JSON.stringify(model));
    });
}

let fillModelModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-model');
    label.html('Модел');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-model' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
    label = label.clone();
    label.attr('for', 'modal-brand')
    label.html('Марка');
    modalBody.append(label);
    let brandSelect = $('<select>');
    brandSelect.addClass('custom-select');
    brandSelect.attr('id', 'modal-brand');
    modalBody.append(brandSelect);
    doGet('brand/get/all', fillBrandComboBox);
}
let fillBrandComboBox = (brands) => fillComboBox('#modal-brand', brands);
let fillModelInTable = function(models){
    createTableHead(['Модел', 'марка', 'Страна на производство', '', '']);
    models.forEach(function(model){
        addTableRow([model.title, model.brand.name, model.brand.countryOfProduction.name,
                    getModelUpdateButton(model), getModelDeletaButton(model.id)]);
    });
}
var fillModels = function(){
    $('#admin-head').html('Модели');
    clearTableRows();
    doGet('model/get/all', fillModelInTable);
    fillModelModal();
    saveModel();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveModel();
    })
}