let getFuelUpdateButton = function(fuel){
    return createUpdatebutton(function () {
        updateFuelModal(fuel);
        $('#btn-add').trigger('click');
    });

}
let getFuelDeletaButton = function(fuelId){
    return createDeletebutton(function(){
        doDelete('fuel/delete/by/id/?id=' + fuelId,function(isDelete){
            if(isDelete){
                fillFuel();
            }else{
                alert('this item has a references!')
            }
        });
    });
}
let saveFuel = function () {
    let typeInput = $('#modal-fuel-type');
    typeInput.val('');
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let fuel = {};
        fuel.type = typeInput.val();
        doPost('fuel/insert', function (fuel) {
            $('#btn-cancel').trigger('click');
            fillFuel();
        }, JSON.stringify(fuel));
    });
}
let updateFuelModal = function (fuel) {
    let typeInput = $('#modal-fuel-type');
    typeInput.val(fuel.type);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        fuel.type = typeInput.val();
        doPut('fuel/update', function (fuel) {
            $('#btn-cancel').trigger('click');
            fillFuel();
        }, JSON.stringify(fuel));
    });
}

let fillFuelModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-fuel-type');
    label.html('Тип на горивото');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-fuel-type' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
}
let fillFuelInTable = function(fuels){
    createTableHead(['Тип на горивото', '', '']);
    fuels.forEach(function(fuel){
        addTableRow([fuel.type, getFuelUpdateButton(fuel), getFuelDeletaButton(fuel.id)]);
    });
}
var fillFuel = function(){
    $('#admin-head').html('Горива');
    clearTableRows();
    doGet('fuel/get/all', fillFuelInTable);
    fillFuelModal();
    saveFuel();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveFuel();
    });
}