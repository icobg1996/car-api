let getCoupeTypeUpdateButton = function(coupeType){
    return createUpdatebutton(function () {
        updatCoupeTypeModal(coupeType);
        $('#btn-add').trigger('click');
    });

}
let getCoupeTypeDeletaButton = function(coupeTypeId){
    return createDeletebutton(function(){
        doDelete('coupe/type/delete/by/id/?id=' + coupeTypeId,function(isDelete){
            if(isDelete){
                fillCoupeType();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveCoupeType = function () {
    let typeInput = $('#modal-type');
    typeInput.val('');
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let coupeType = {};
        coupeType.type = typeInput.val();
        doPost('coupe/type/insert', function (coupeType) {
            $('#btn-cancel').trigger('click');
            fillCoupeType();
        }, JSON.stringify(coupeType));
    });
}
let updatCoupeTypeModal = function (coupeType) {
    let typeInput = $('#modal-type');
    typeInput.val(coupeType.type);
    console.log(coupeType.type);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        coupeType.type = typeInput.val();
        doPut('coupe/type/update', function (coupeType) {
            $('#btn-cancel').trigger('click');
            fillCoupeType();
        }, JSON.stringify(coupeType));
    });
}

let fillCoupeTypeModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-type');
    label.html('Тип на купето');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-type' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
}
let fillCoupeTypeInTable = function(coupeTypes){
    createTableHead(['Тип на купето', '', '']);
    coupeTypes.forEach(function(coupeType){
        addTableRow([coupeType.type, getCoupeTypeUpdateButton(coupeType), getCoupeTypeDeletaButton(coupeType.id)]);
    });
}
var fillCoupeType = function(){
    $('#admin-head').html('Типове на купето');
    clearTableRows();
    doGet('coupe/type/get/all', fillCoupeTypeInTable);
    fillCoupeTypeModal();
    saveCoupeType();
    $('#exampleModal').on('hidden.bs.modal', function(){
        saveCoupeType();
    });
}