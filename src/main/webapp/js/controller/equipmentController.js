let getEquipmentUpdateButton = function(equipment){
    return createUpdatebutton(function () {
        updatEquipmentModal(equipment);
        $('#btn-add').trigger('click');
    });

}
let getEquipmentDeletaButton = function(equipmentId){
    return createDeletebutton(function(){
        doDelete('equipment/delete/by/id/?id=' + equipmentId,function(isDelete){
            if(isDelete){
                fillEquipment();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveEquipment = function () {
    let nameInput = $('#modal-equipment');
    nameInput.val('');
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let equipment = {};
        equipment.name = nameInput.val();
        doPost('equipment/insert', function (equipment) {
            $('#btn-cancel').trigger('click');
            fillEquipment();
        }, JSON.stringify(equipment));
    });
}
let updatEquipmentModal = function (equipment) {
    let nameInput = $('#modal-equipment');
    nameInput.val(equipment.name);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        equipment.name = nameInput.val();
        doPut('equipment/update', function (equipment) {
            $('#btn-cancel').trigger('click');
            fillEquipment();
        }, JSON.stringify(equipment));
    });
}

let fillEquipmentModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-equipment');
    label.html('Име на оборудването');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-equipment' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
}
let fillEquipmentInTable = function(equipments){
    createTableHead(['Име на оборудването', '', '']);
    equipments.forEach(function(equipment){
        addTableRow([equipment.name, getEquipmentUpdateButton(equipment), getEquipmentDeletaButton(equipment.id)]);
    });
}
var fillEquipment = function(){
    $('#admin-head').html('Оборудвания');
    clearTableRows();
    doGet('equipment/get/all', fillEquipmentInTable);
    fillEquipmentModal();
    saveEquipment();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveEquipment();
    });
}