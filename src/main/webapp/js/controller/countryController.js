let getCountryUpdateButton = function(country){
    return createUpdatebutton(function () {
        updatCountryeModal(country);
        $('#btn-add').trigger('click');
    });

}
let getCountryDeletaButton = function(countryId){
    return createDeletebutton(function(){
        doDelete('country/delete/by/id/?id=' + countryId,function(isDelete){
            if(isDelete){
                fillCountry();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveCountry = function () {
    let nameInput = $('#modal-country-name');
    nameInput.val('');
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let country = {};
        country.name = nameInput.val();
        doPost('country/insert', function (country) {
            $('#btn-cancel').trigger('click');
            fillCountry();
        }, JSON.stringify(country));
    });
}
let updatCountryeModal = function (country) {
    let nameInput = $('#modal-country-name');
    nameInput.val(country.name);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        country.name = nameInput.val();
        doPut('country/update', function (country) {
            $('#btn-cancel').trigger('click');
            fillCountry();
        }, JSON.stringify(country));
    });
}

let fillCountryModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-country-name');
    label.html('Име на Държавата');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-country-name' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
}
let fillCountryInTable = function(countris){
    createTableHead(['Име на държавата', '', '']);
    countris.forEach(function(country){
        addTableRow([country.name,getCountryUpdateButton(country), getCountryDeletaButton(country.id)]);
    });
}
var fillCountry = function(){
    $('#admin-head').html('Държави');
    clearTableRows();
    doGet('country/get/all', fillCountryInTable);
    fillCountryModal();
    saveCountry();
    $('#exampleModal').on('hidden.bs.modal', function(){
        saveCountry();
    });
}