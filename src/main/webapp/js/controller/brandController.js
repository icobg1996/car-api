let getUpdateButton = function(brand){
    return createUpdatebutton(function () {
        updateModal(brand);
        $('#btn-add').trigger('click');
    });

}
let getDeletaButton = function(brandId){
    return createDeletebutton(function(){
        doDelete('brand/delete/by/id/?id=' + brandId,function(isDelete){
            if(isDelete){
                fillBrands();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveBrand = function () {
    let nameInput = $('#modal-brand');
    nameInput.val('');
    let countryInput = $('#modal-country');
    countryInput.val("-1")
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let brand = {};
        brand.name = nameInput.val();
        brand.countryOfProduction = { id: parseInt(countryInput.val())};
        doPost('brand/insert', function (brandResp) {
            $('#btn-cancel').trigger('click');
            fillBrands();
        }, JSON.stringify(brand));
    });
}
let updateModal = function (brand) {
    let nameInput = $('#modal-brand');
    nameInput.val(brand.name);
    let countryInput = $('#modal-country');
    countryInput.val(brand.countryOfProduction.id);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        brand.name = nameInput.val();
        brand.countryOfProduction = { id: parseInt(countryInput.val()) };
        doPut('brand/update', function (brand) {
            $('#btn-cancel').trigger('click');
            fillBrands();
        }, JSON.stringify(brand));
    });
}
let fillBrandsInTable = function (brands) {
    createTableHead(['Марка', 'Страна на производство', '', '']);
    brands.forEach(function (brand) {
        addTableRow([brand.name, brand.countryOfProduction.name, getUpdateButton(brand), getDeletaButton(brand.id)]);
    });
}
let fillcountryComboBox = (countris) => fillComboBox('#modal-country', countris);

let fillModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-brand');
    label.html('Име на марката');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-brand' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
    label = label.clone();
    label.attr('for', 'modal-country')
    label.html('Държава');
    modalBody.append(label);
    let countrySelect = $('<select>');
    countrySelect.addClass('custom-select');
    countrySelect.attr('id', 'modal-country');
    modalBody.append(countrySelect);
    doGet('country/get/all', fillcountryComboBox);
}
var fillBrands = function () {
    $('#admin-head').html('Марки');
    clearTableRows();
    doGet('brand/get/all', fillBrandsInTable);
    fillModal();
    saveBrand();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveBrand();
    });
}
