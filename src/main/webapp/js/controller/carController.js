let getEquipmantsDropdown = function(equipments){
    let btnGroup = $('<div>');
    btnGroup.addClass('btn-group');
    let dropdownBtn = $('<button>');
    dropdownBtn.addClass('btn btn-sm dropdown-toggle');
    dropdownBtn.attr({'type' : 'button',
                    'data-toggle' : 'dropdown',
                    'aria-haspopup' : 'true',
                    'aria-expanded' : 'false'});
    dropdownBtn.html('Оборудване');
    btnGroup.html(dropdownBtn);
    let divDropdown = $('<div>');
    divDropdown.addClass('dropdown-menu');
    btnGroup.append(divDropdown);
    equipments.forEach(function(element){
        let span = $('<span>');
        span.addClass('dropdown-item');
        span.html(element.name);
        divDropdown.append(span);
    });
    return btnGroup;
}
let fillCarInTable = function(cars){
    createTableHead(['Марка', 'Модел', 'Година на производство', 'Конски сили', 'Скоростна кутия', 
    'Гориво', 'Пробег', 'Тип на купето', 'Цвят', 'Оборудване', 'Описание', 'Снимка', '', '']);
    cars.forEach(function(car){
        addTableRow([car.model.brand.name,
        car.model.title,
        car.productionYear,
        car.horsePower,
        car.transmission.type,
        car.fuel.type,
        car.mileage,
        car.coupeType.type,
        car.color,
        getEquipmantsDropdown(car.equipment),
        car.description,
        car.imagePath,
        getCarUpdateButton(car),
        getCarDeletaButton(car.id)
        ]);
    });
}
var fillCar = function(){
    $('#admin-head').html('Коли');
    clearTableRows();
    doGet('car/get/all', fillCarInTable);
    fillCarModal();
    saveCar();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveCar();
    });
}

let createDropDowns = function(modalBody, objs){
    objs.forEach(function(obj){
        let select = $('<select>');
        let label = $('<label>');
        label.attr('for', 'modal-' + obj.id);
        label.append(obj.label);
        if(!obj.multi) {
            select.addClass('custom-select');
        }else{
            select.attr('multiple','');
        }
        select.attr('id', 'modal-' + obj.id);
        modalBody.append(label);
        modalBody.append(select);
    });

}
let createInputs = function(modalBody, objs){
    objs.forEach(function(obj){
        let label = $('<label>');
        label.attr('for', 'modal-' + obj.id);
        label.html(obj.label);
        modalBody.append(label);
        let input = $('<input>');
        if(obj.attr != null){
            input.attr(obj.attr);
        }
        input.attr('id', 'modal-' + obj.id);
        input.addClass('form-control');
        let inputGroup = $('<div>');
        inputGroup.addClass('input-group');
        inputGroup.append(input);
        modalBody.append(inputGroup);
    });
}
let fillCarModal = function () {
    let modalBody = $('#modal-body');
    createDropDowns(modalBody, [{id : 'brand', label : 'Марка'},
                            {id : 'model', label : 'Модел'},
                            {id : 'transmission', label : 'Скоростна кутия'},
                            {id : 'fuel', label : 'Гориво'},
                            {id : 'coupe-type', label : 'Тип на купето'},
                            {id : 'equipment', label : 'Оборудване', multi : true}]);

    createInputs(modalBody, [{id : 'year', label : 'Година на производство', attr : {type : 'text', readonly : '', placeholder : 'Година'}},
                             {id : 'hpower', label : "Конски сили", attr : {type : 'number'}},
                             {id : 'mileage', label : 'Пробег', attr : {type : 'number'}},
                             {id : 'color', label : 'Цвят'},
                             {id : 'description', label : 'Описание'}]);

    doGet('brand/get/all', fillBrandModalComboBox);
    doGet('transmission/get/all', fillTransmissionModalComboBox);
    doGet('fuel/get/all', fillFuelModalComboBox);
    doGet('coupe/type//get/all', fillCoupeTypeModalComboBox);
    doGet('equipment/get/all', fillEquipmentModalComboBox);
    $("#modal-brand").change(function () {
        var brandId = parseInt($("#modal-brand").val());
        if (isNaN(brandId)) {
            fillComboBox('#modal-brand', [], 'Избери първо марка...');
            return;
        }
        doGet('model/get/by/brand/id', fillModelModalComboBox, { brandId: brandId });
    });
    $('#modal-year').datepicker({
        format: 'yyyy',
        viewMode: "years",
        minViewMode: "years"
    });
}

let fillBrandModalComboBox = (brands) => fillComboBox('#modal-brand', brands);
let fillModelModalComboBox = (models) => fillComboBox('#modal-model', models, 'Избери...');
let fillTransmissionModalComboBox = (transmissions) => fillComboBox('#modal-transmission', transmissions);
let fillFuelModalComboBox = (fuels) => fillComboBox('#modal-fuel', fuels);
let fillCoupeTypeModalComboBox = (coupeTypes) => fillComboBox('#modal-coupe-type', coupeTypes);
let fillEquipmentModalComboBox = (equipments) => fillMultiSelectComboBox('#modal-equipment', equipments);

let mapCar = function (car){
    let newCar = car != null ? car : {};
    newCar.productionYear = $('#modal-year').val();
    newCar.horsePower = $('#modal-hpower').val();
    newCar.mileage = $('#modal-mileage').val();
    newCar.color = $('#modal-color').val();
    newCar.description = $('#modal-description').val();
    newCar.model = {id : parseInt($('#modal-model').val())};
    newCar.fuel = {id : parseInt($('#modal-fuel').val())};
    //newCar.equipment = {id : parseInt($('#modal-equipment').val())};
    newCar.coupeType = {id : parseInt($('#modal-coupe-type').val())};
    newCar.transmission = {id : parseInt($('#modal-transmission').val())};
    return newCar;
}
let fillFields = function (car){
    $('#modal-year').val(car.productionYear);
    $('#modal-hpower').val(car.horsePower);
    $('#modal-mileage').val(car.mileage);
    $('#modal-color').val(car.color);
    $('#modal-description').val(car.description);
    $('#modal-brand').val(car.model.brand.id);
    //$('#modal-brand').trigger('change');
    $('#modal-fuel').val(car.fuel.id);
    //$('#modal-equipment').val(car.equipment);
    $('#modal-coupe-type').val(car.coupeType.id);
    $('#modal-transmission').val(car.transmission.id);
    $('#modal-model').val(car.model.id);
}
let clearFields = function(){
    $('#modal-year').val('');
    $('#modal-hpower').val(0);
    $('#modal-mileage').val(0);
    $('#modal-color').val('');
    $('#modal-description').val('');
    $('#modal-brand').val('-1');
    $('#modal-model').val('-1');
    $('#modal-fuel').val('-1');
    //$('#modal-equipment').val(car.equipment);
    $('#modal-coupe-type').val('-1');
    $('#modal-transmission').val('-1');
}
let getCarUpdateButton = function(car){
    return createUpdatebutton(function () {
        updateCar(car);
        $('#btn-add').trigger('click');
    });

}
let getCarDeletaButton = function(carId){
    return createDeletebutton(function(){
        doDelete('car/delete/by/id/?id=' + carId,function(isDelete){
            if(isDelete){
                fillCar();
            }else{
                alert('this item has a references!')
            }
        });
    })
}
let saveCar = function () {
    clearFields();
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let car = mapCar(null);
        doPost('car/insert', function (car) {
            $('#btn-cancel').trigger('click');
            fillCar();
        }, JSON.stringify(car));
    });
}
let updateCar = function (car) {
    fillFields(car);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let newCar = mapCar(car);
        doPut('car/update', function (car) {
            $('#btn-cancel').trigger('click');
            fillBrands();
        }, JSON.stringify(newCar));
    });
}