let getTransmissionUpdateButton = function(transmission){
    return createUpdatebutton(function () {
        updateTransmissionModal(transmission);
        $('#btn-add').trigger('click');
    });

}
let getTransmissionDeletaButton = function(transmissionId){
    return createDeletebutton(function(){
        doDelete('transmission/delete/by/id/?id=' + transmissionId,function(isDelete){
            if(isDelete){
                fillTransmission();
            }else{
                alert('this item has a references!')
            }
        });
    });
}
let saveTransmission = function () {
    let typeInput = $('#modal-transmission');
    typeInput.val('');
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        let transmission = {};
        transmission.type = typeInput.val();
        doPost('transmission/insert', function (transmission) {
            $('#btn-cancel').trigger('click');
            fillTransmission();
        }, JSON.stringify(transmission));
    });
}
let updateTransmissionModal = function (transmission) {
    let typeInput = $('#modal-transmission');
    typeInput.val(transmission.type);
    $('#save-update-btn').off('click');
    $('#save-update-btn').click(function () {
        transmission.type = typeInput.val();
        doPut('transmission/update', function (transmission) {
            $('#btn-cancel').trigger('click');
            fillTransmission();
        }, JSON.stringify(transmission));
    });
}

let fillTransmissionModal = function () {
    let modalBody = $('#modal-body');
    let label = $('<label>');
    label.attr('for', 'modal-transmission');
    label.html('Тип на скоростната кутия');
    modalBody.html(label);
    let inputGroup = $('<div>');
    inputGroup.addClass('input-group');
    let input = $('<input>');
    input.addClass('form-control')
    input.attr({ type: 'text', id: 'modal-transmission' });
    inputGroup.html(input);
    modalBody.append(inputGroup);
}

let fillTransmissionInTable = function(transmissions){
    createTableHead(['Тип на скоростната кутия ', '', '']);
    transmissions.forEach(function(transmission){
        addTableRow([transmission.type, getTransmissionUpdateButton(transmission), getTransmissionDeletaButton(transmission.id)]);
    });
}
var fillTransmission = function(){
    $('#admin-head').html('Типове на скоростни кутии');
    clearTableRows();
    doGet('transmission/get/all', fillTransmissionInTable);
    fillTransmissionModal();
    saveTransmission();
    $('#exampleModal').on('hidden.bs.modal', function () {
        saveTransmission();
    });
}