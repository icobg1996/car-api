var createTableHead = function(columNames){
    let headRow = $('<tr>');
    columNames.forEach(function(columName){
        let headCell = $('<th>');
        headCell.html(columName);
        headRow.append(headCell);
    });
    $('#admin-table > thead').html(headRow);
}
let createTableRow = function(values){
    let bodyRow = $('<tr>');
    values.forEach(function(value){
        let bodyCell = $('<td>');
        bodyCell.html(value);
        bodyRow.append(bodyCell);
    });
    return bodyRow;
}
var addTableRow = (values) => $('#admin-table > tbody').append(createTableRow(values));
var clearTableRows = () => $('#admin-table > tbody').html('');
var createDeletebutton = function (clickFunction) {
    let updateDeleteButton = $('<button>');
    updateDeleteButton.attr('type', 'button');
    updateDeleteButton.addClass('btn btn-danger btn-sm');
    updateDeleteButton.html('Изтрий')
    updateDeleteButton.click(clickFunction);
    return updateDeleteButton;
}
var createUpdatebutton = function (clickFunction) {
    let updateButton = $('<button>');
    updateButton.attr('type', 'button');
    updateButton.addClass('btn btn-primary btn-sm');
    updateButton.html('Ъпдейт')
    updateButton.click(clickFunction);
    return updateButton;
}
