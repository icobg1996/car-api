let width = 50;
let fillBrandComboBox = (brands) => fillComboBox('#brand-input', brands);
let fillModelComboBox = (models) => fillComboBox('#model-input', models, 'Избери...');
let fillTransmissionComboBox = (transmissions) => fillMultiSelectComboBox('#transmission-input', transmissions);
let fillFuelComboBox = (fuels) => fillMultiSelectComboBox('#fuel-input', fuels);
let fillCoupeTypeComboBox = (coupeTypes) => fillMultiSelectComboBox('#coupe-type-input', coupeTypes);
let fillEquipmentComboBox = (equipments) => fillMultiSelectComboBox('#equipment-input', equipments);
let animateShadow = function (element) {
    element.mouseover(function () {
        element.addClass('shadow');
    });
    element.mouseout(function () {
        element.removeClass('shadow');
    });
}
let createListItem = function (content) {
    let listItem = $('<li>');
    listItem.addClass('list-group-item');
    listItem.html(content);
    animateShadow(listItem);
    return listItem;
}
let createCarItem = function (car) {
    let row = $('<div>');
    row.addClass('row p-2 mt-2 shadow-sm rounded bg-white w-' + width + ' car-item');
    let image = $('<img>');
    image.addClass('img-fluid');
    animateShadow(image);
    image.attr('src', car.imagePath);
    let list = $('<ul>');
    list.addClass('list-group list-group-flush');
    list.html(createListItem('<b>Марка :</b> ' + car.model.brand.name))
        .append(createListItem('<b>Модел :</b> ' + car.model.title))
        .append(createListItem('<b>Гориво :</b> ' + car.fuel.type))
        .append(createListItem('<b>Година :</b> ' + car.productionYear))
        .append(createListItem('<b>Мощност :</b> ' + car.horsePower))
        .append(createListItem('<b>Пробег :</b> ' + car.mileage))
        .append(createListItem('<b>Скоростна кутия :</b> ' + car.transmission.type))
        .append(createListItem('<b>Тип на купето :</b> ' + car.coupeType.type))
        .append(createListItem('<b>Цвят :</b> ' + car.color));
    let leftColumn = $('<div>');
    leftColumn.addClass('col col-6');
    leftColumn.html(image);
    let paragraph = $('<p>');
    paragraph.html('<b>Описание :</b> <br>' + car.description);
    paragraph.addClass('mt-3 p-2 bg-light rounded');
    animateShadow(paragraph);
    leftColumn.append(paragraph);
    row.html(leftColumn);
    let rightColumn = $('<div>');
    rightColumn.addClass('col col-6');
    rightColumn.html(list);
    row.append(rightColumn);
    return row;
}
let getFilter = function () {
    let filter = {};
    let modelId = $('#model-input').val();
    let transmissionIds = $('#transmission-input').val();
    let fuelIds = $('#fuel-input').val();
    let coupeTypeIds = $('#coupe-type-input').val();
    let equipmentIds = $('#equipment-input').val();
    let year = $('#year-input').val();
    if (modelId != null && modelId != '' && modelId != '-1') {
        filter.modelIds = modelId;
    }
    if (transmissionIds.length != 0) {
        filter.transmissionIds = transmissionIds.toString();
    }
    if (fuelIds.length != 0) {
        filter.fuelIds = fuelIds.toString();
    }
    if (coupeTypeIds.length != 0) {
        filter.coupeTypeIds = coupeTypeIds.toString();
    }
    if (equipmentIds.length != 0) {
        filter.equipmentIds = equipmentIds.toString();
    }
    if (year != null && year != '') {
        filter.productionYear = year;
    }
    return filter;
}

$(document).ready(function () {
    doGet('/brand/get/all', fillBrandComboBox);
    doGet('/equipment/get/all', fillEquipmentComboBox);
    doGet('/coupe/type/get/all', fillCoupeTypeComboBox);
    doGet('/fuel/get/all', fillFuelComboBox);
    doGet('/transmission/get/all', fillTransmissionComboBox);

    $("#brand-input").change(function () {
        var brandId = parseInt($("#brand-input").val());
        if (isNaN(brandId)) {
            fillComboBox('#model-input', [], 'Избери първо марка...');
            return;
        }
        doGet('model/get/by/brand/id', fillModelComboBox, { brandId: brandId });
    });
    $('#btn-filter').click(function () {
        doGet('/car/get/all', function (cars) {
            cars.forEach(function (element) {
                $('#content').append(createCarItem(element));
            });
        }, getFilter());
    });
    $('#btn-reorder').click(function () {
        let oldWidth = width;
        if (width == 50) {
            width = 100;
        } else {
            width = 50;
        }
        $('.car-item').each(function () {
            $(this).removeClass('w-' + oldWidth);
            $(this).addClass('w-' + width);
        });
    });
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $('#year-input').datepicker({
        format: 'yyyy',
        viewMode: "years",
        minViewMode: "years"
    });
});
