function createOption(val, text){
    let option = $('<option>');
    if(val != null){
        option.val(val);
    }
    if(text != null){
        option.text(text);
    }
    return option;
}
function addOptionbyId(id, val, text){
    let select = $(id);
    select.append(createOption(val,text));
}
var fillComboBox = function(id,items,caption){
    if(caption != ''){
        $(id).html(createOption(-1, caption != null ? caption : 'Избери...'));
    }
    items.forEach(function(element) {
        if(element.name != null){
            addOptionbyId(id, element.id, element.name);
        }else if(element.title != null){
            addOptionbyId(id, element.id, element.title);
        }else if(element.type != null){
            addOptionbyId(id, element.id, element.type);
        }
    });
}
var fillMultiSelectComboBox = function (id,items){
    fillComboBox(id,items,'');
    $(id).chosen();
}